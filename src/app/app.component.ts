import { Component, OnInit } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import { AuthenticationResult } from '@azure/msal-common';

export class userModel {
  oid: String = "";
  name: String = "";
  username: String = "";
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'geo-devis';

  user:any = new userModel
  sidebar:boolean = false;
  constructor(private msalService: MsalService) {
    
  }

  isLoggedIn() : boolean {
    return this.msalService.instance.getActiveAccount() != null
  }

  getUserInstance() {
    return this.msalService.instance.getActiveAccount()?.name
  }
  getUserInstanceInfo() {
    let info = this.msalService.instance.getActiveAccount()?.name + "\r" + this.msalService.instance.getActiveAccount()?.username
    return info
  }

  login () {
    this.msalService.loginPopup().subscribe((response: AuthenticationResult) => {
      this.msalService.instance.setActiveAccount(response.account);
      var idTokenClaims:any = response.account?.idTokenClaims
      this.user = {
        oid: idTokenClaims.oid || "",
        name:response.account?.name || "",
        username: response.account?.username || "",
      }
    })
  }

  logout () {
    this.msalService.logout();
  }

  toogleSidebar() {
    if(this.sidebar) {
      (<HTMLElement>document.getElementById("left-bar")).style.width = "45px";
      this.sidebar = false
    } else {
      this.sidebar = true;
      (<HTMLElement>document.getElementById("left-bar")).style.width = "150px";

    }
  }



  ngOnInit(): void {

  }
  


}