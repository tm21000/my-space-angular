import {  OnInit } from '@angular/core';
import {
  NgModule, ViewChild, Component, enableProdMode,
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {
  DxSchedulerModule,
  DxSchedulerComponent,
  DxTemplateModule,
  DxSwitchModule,
  DxNumberBoxModule,
} from 'devextreme-angular';
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  // @ViewChild(DxSchedulerComponent, { static: false }) scheduler: DxSchedulerComponent;

  constructor() { }
  currentDate: Date = new Date();
  showCurrentTimeIndicator = true;
  intervalValue = 10;
  indicatorUpdateInterval = 10000;
  shadeUntilCurrentTime = true;
  changeIndicatorUpdateInterval(e:any) {
    this.indicatorUpdateInterval = e.value * 1000;
  }
  ngOnInit(): void {
  }

}
