import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { environment } from '../environments/environment';
import { MsalModule, MsalService, MSAL_INSTANCE } from '@azure/msal-angular';
import { IPublicClientApplication, PublicClientApplication } from '@azure/msal-browser';

import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { DxNumberBoxModule, DxSchedulerModule, DxSwitchModule, DxTemplateModule } from 'devextreme-angular';

export function MSALInstanceFactory(): IPublicClientApplication {
  return new PublicClientApplication({
    auth: {      
      clientId: environment.clientId,
      redirectUri: environment.redirectUri,
      authority: "https://login.microsoftonline.com/geotec.fr"
    }
  })
}

@NgModule({
  declarations: [
    AppComponent,
    routingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DxSchedulerModule,
    DxSwitchModule,
    DxNumberBoxModule,
    DxTemplateModule,
  ],
  providers: [{
    provide: MSAL_INSTANCE,
    useFactory: MSALInstanceFactory
  },
  MsalService,  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
