import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalendarComponent } from './calendar/calendar.component';
import { HomeComponent } from './home/home.component';
import { TabComponent } from './tab/tab.component';
import { ToolsComponent } from './tools/tools.component';

const routes: Routes = [ 
  {path: '', component: HomeComponent},
  {path: 'tools', component: ToolsComponent},
  {path: 'calendar', component: CalendarComponent},
  {path: 'tab', component: TabComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponent = [
  HomeComponent,
  ToolsComponent,
  TabComponent,
  CalendarComponent,
]